#define _CRT_SECURE_NO_WARNINGS 
#include <bits/stdc++.h>
#include <time.h>;
using std::vector; 
typedef unsigned long long llu;
llu max_number = 10001;
vector<llu> prime, result; //소수 리스트

bool isPrime(llu n) {
    for (llu i = max_number - 10000 + 1; i <= sqrt(n); i++) //전달받은 n의 제곱근 루트n을
        //루트n 보다 작은 정수 i들로 나누었을때 나눠떨어지면
        if (n % i == 0)
            return false; //소수가아니다.
    return true; //만약 모든 i가 나눠떨어지지않으면 소수인것
}
void find_prime(int n) { //n만개의 정수를 탐색하여 소수만 prime에 기록ㄴ
    while (n--) {
        for (llu i = max_number - 10000 + 1; i <= max_number; i++)
            if (isPrime(i))
                prime.push_back(i);
        max_number += 10000;
    }
}

void init() {
    find_prime(1); //처음엔 만개의 소수를 탐색

}

void logic(llu n) {
    double start_time = (double)clock() / CLOCKS_PER_SEC;
    if (n == 2) {
        double end_time = (double)clock() / CLOCKS_PER_SEC;
        printf("2  3  등차 : 1, 소요시간 : %.4f\n\n", end_time - start_time);
        return;
    }
    llu start = 0;
    llu go = 0; //소수리스트의 시작 인덱스
    llu chunk = 0; //소수를 건너띌때 사용할 인덱스
    llu diff = 2; //소수등차수열의 등차. 최솟값
    llu to = prime.size(); //소수리스트의 마지막 인덱스
    bool end = false;
    if (n >= 11 && max_number == 10001) {
        find_prime(10);
    }

    while (1) {
        if (go + 1 >= to || end) {  //현재 등차값으로 탐색을 완료했으면
            diff++; //등차값을 증가시키고
            start = 0;
            go = 0;  //다시 반복한다.
            result.clear();
            if (diff >= max_number) //등차값이 너무커지면 소수리스트를 갱신함.
                find_prime(3);
            to = prime.size();
            end = false;
        }

        if (result.size() == 0) {
            start++;
            go = start;
            result.push_back(prime[go]);
            continue;
        }
        if (result.size() == n) { //n개의 소수등차수열을 찾았다면
            for (llu i = 0; i < n; i++) { //출력
                printf("%llu  ", result[i]);
            }
            result.clear();
            double end_time = (double)clock() / CLOCKS_PER_SEC;
            printf("등차 : %llu, 소요시간 : %.4f\n\n", diff, end_time - start_time); //출력
            return;
        }
        /////////////////////////////////////////////////////////////////
        //이전소수와 현재소수의 차잇값과 등차값을 비교
        if (prime[go + 1] - prime[go] == diff) { //같을때
            go++;
            result.push_back(prime[go]);
        }
        else if (prime[go + 1] - prime[go] > diff) { //차잇값이 더 클때
            if (result.size() == 1)
                go++;
            result.clear();
        }
        else { /////////////////////////////////////////////////////////////
            //등차값이 더 클때
            //현재소수 다음소수와 차잇값으로 등차값과 비교
            chunk = go + 2;
            while (1) {
                if (chunk >= to) {
                    end = true;
                    break;
                }
                if (prime[chunk] - prime[go] == diff) { //같을때
                    result.push_back(prime[chunk]); //찾은소수를 기록
                    go = chunk; //현재 찾은 소수를 이전소수로 기록(계속 반복할것이니까)
                    break; //찾은 소수를 기록
                }
                else if (prime[chunk] - prime[go] > diff) { //차잇값이 더 클때
                    go = chunk - 1;
                    result.clear();
                    break; //지금껏 찾은 소수들을 초기화하고 다시 진행한다.
                }
                else { //등차값이 더 클때
                    chunk++;
                }
            }
        }
    }
}

void func() {
    while (1) {
        printf("%s", "정수n 을 입력시 길이가 n인 소수등차수열을 출력합니다.");
        llu n;
        scanf("%llu", &n);
        logic(n);
    }

}

int main(void) {
    init();
    func();

    return 0;
}